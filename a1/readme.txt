1. the server code should be put in rb1.cs.unh.edu
2. and the client code should be put in rb2.cs.unh.edu, because the interface IP of rb1 are considered as server address and are hard coded in the client code.
3. the command line of setting up server are as follow:
	[gpw6@rb1 ~]$ python tcp_server.py 
	[gpw6@rb1 ~]$ python udp_server.py 
	[gpw6@rb1 ~]$ node nodes.js
4. once the server is set up correctly, there should be some welcome information.
5. the command line of run client code:
	[gpw6@rb2 cs925_hw1]$ python client.py 
6. the client should run all four protocol on four interface, and print the result on screen
7. the result of example run is also attached
