var express = require('express'); 
var fs = require('fs');
var https = require('https');
var http = require('http');
var url = require("url");
var app = express();

var options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

app.get('/formnjs.html', function(req, res) {    
    res.sendFile('formnjs.html', {root: __dirname });
    console.log('send html');
});

http.createServer(app).listen(8579);
https.createServer(options, app).listen(8576);

