from socket import *
from time import sleep
from time import time
import sys
import numpy as np
import requests

# ------------ TCP ------------
print "------------ TCP ------------"
port = 8889
buf=1024

data = """test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time
    test info for TCP transfer and response time"""

#host="132.177.11.131"
host="192.168.0.1"
tcp1 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_STREAM)
    s.connect((host,port))
    s.send(data)
    t2 = s.recv(buf)
    s.close()
    t4 = time()
    tcp1[x] = (t4 - t1)*1000
print "tcp test result for p4p1"
print "std", np.std(tcp1)
print "mean", np.mean(tcp1)
print "min", np.min(tcp1)
print "max", np.max(tcp1)
print "steps", 100
print " "

host="192.168.0.5"
tcp2 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_STREAM)
    s.connect((host,port))
    s.send(data)
    t2 = s.recv(buf)
    s.close()
    t4 = time()
    tcp2[x] = (t4 - t1)*1000
print "tcp test result for p4p2"
print "std", np.std(tcp2)
print "mean", np.mean(tcp2)
print "min", np.min(tcp2)
print "max", np.max(tcp2)
print "steps", 100
print " "

host="192.168.0.9"
tcp3 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_STREAM)
    s.connect((host,port))
    s.send(data)
    t2 = s.recv(buf)
    s.close()
    t4 = time()
    tcp3[x] = (t4 - t1)*1000
print "tcp test result for p4p3"
print "std", np.std(tcp3)
print "mean", np.mean(tcp3)
print "min", np.min(tcp3)
print "max", np.max(tcp3)
print "steps", 100
print " "

host="192.168.0.13"
tcp4 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_STREAM)
    s.connect((host,port))
    s.send(data)
    t2 = s.recv(buf)
    s.close()
    t4 = time()
    tcp4[x] = (t4 - t1)*1000
print "tcp test result for p4p4"
print "std", np.std(tcp4)
print "mean", np.mean(tcp4)
print "min", np.min(tcp4)
print "max", np.max(tcp4)
print "steps", 100
print " "

# ------------ UDP ------------
print "------------ UDP ------------"

data = """
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time
    test info for UDP transfer and response time"""

host="192.168.0.1"
port = 8887
addr = (host,port)
udp1 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_DGRAM)
    s.sendto(data,addr)
    t2,addr = s.recvfrom(buf)
    s.close()
    t4 = time()
    udp1[x] = (t4 - t1)*1000
print "UDP test result for p4p1"
print "std", np.std(udp1)
print "mean", np.mean(udp1)
print "min", np.min(udp1)
print "max", np.max(udp1)
print "steps", 100
print " "

host="192.168.0.5"
port = 8887
addr = (host,port)
udp2 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_DGRAM)
    s.sendto(data,addr)
    t2,addr = s.recvfrom(buf)
    s.close()
    t4 = time()
    udp2[x] = (t4 - t1)*1000
print "UDP test result for p4p2"
print "std", np.std(udp2)
print "mean", np.mean(udp2)
print "min", np.min(udp2)
print "max", np.max(udp2)
print "steps", 100
print " "

host="192.168.0.9"
port = 8887
addr = (host,port)
udp3 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_DGRAM)
    s.sendto(data,addr)
    t2,addr = s.recvfrom(buf)
    s.close()
    t4 = time()
    udp3[x] = (t4 - t1)*1000
print "UDP test result for p4p3"
print "std", np.std(udp3)
print "mean", np.mean(udp3)
print "min", np.min(udp3)
print "max", np.max(udp3)
print "steps", 100
print " "

host="192.168.0.13"
port = 8887
addr = (host,port)
udp4 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    s = socket(AF_INET,SOCK_DGRAM)
    s.sendto(data,addr)
    t2,addr = s.recvfrom(buf)
    s.close()
    t4 = time()
    udp4[x] = (t4 - t1)*1000
print "UDP test result for p4p4"
print "std", np.std(udp4)
print "mean", np.mean(udp4)
print "min", np.min(udp4)
print "max", np.max(udp4)
print "steps", 100
print " "

# ------------ HTTP ------------
print "------------ HTTP ------------"
http1 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('http://192.168.0.1:8579/formnjs.html')
    t4 = time()
    http1[x] = (t4 - t1)*1000

print "HTTP test result for p4p1"
print "std", np.std(http1)
print "mean", np.mean(http1)
print "min", np.min(http1)
print "max", np.max(http1)
print "steps", 100
print " "

http2 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('http://192.168.0.5:8579/formnjs.html')
    t4 = time()
    http2[x] = (t4 - t1)*1000
print "HTTP test result for p4p2"
print "std", np.std(http2)
print "mean", np.mean(http2)
print "min", np.min(http2)
print "max", np.max(http2)
print "steps", 100
print " "

http3 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('http://192.168.0.9:8579/formnjs.html')
    t4 = time()
    http3[x] = (t4 - t1)*1000
print "HTTP test result for p4p3"
print "std", np.std(http3)
print "mean", np.mean(http3)
print "min", np.min(http3)
print "max", np.max(http3)
print "steps", 100
print " "

http4 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('http://192.168.0.13:8579/formnjs.html')
    t4 = time()
    http4[x] = (t4 - t1)*1000
print "HTTP test result for p4p4"
print "std", np.std(http4)
print "mean", np.mean(http4)
print "min", np.min(http4)
print "max", np.max(http4)
print "steps", 100
print " "


# ------------ HTTPS ------------
print "------------ HTTPS ------------"
requests.packages.urllib3.disable_warnings()
https1 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('https://192.168.0.1:8576/formnjs.html', verify=False)
    t4 = time()
    https1[x] = (t4 - t1)*1000
print "HTTPS test result for p4p1"
print "std", np.std(https1)
print "mean", np.mean(https1)
print "min", np.min(https1)
print "max", np.max(https1)
print "steps", 100
print " "

https2 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('https://192.168.0.5:8576/formnjs.html', verify=False)
    t4 = time()
    https2[x] = (t4 - t1)*1000
print "HTTPS test result for p4p2"
print "std", np.std(https2)
print "mean", np.mean(https2)
print "min", np.min(https2)
print "max", np.max(https2)
print "steps", 100
print " "

https3 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('https://192.168.0.9:8576/formnjs.html', verify=False)
    t4 = time()
    https3[x] = (t4 - t1)*1000
print "HTTPS test result for p4p3"
print "std", np.std(https3)
print "mean", np.mean(https3)
print "min", np.min(https3)
print "max", np.max(https3)
print "steps", 100
print " "

https4 = np.zeros(100)
for x in range(0,100):
    sleep(0.1)
    t1 = time()
    requests.get('https://192.168.0.13:8576/formnjs.html', verify=False)
    t4 = time()
    https4[x] = (t4 - t1)*1000
print "HTTPS test result for p4p4"
print "std", np.std(https4)
print "mean", np.mean(https4)
print "min", np.min(https4)
print "max", np.max(https4)
print "steps", 100
print " "



