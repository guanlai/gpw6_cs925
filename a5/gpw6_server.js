var express = require('express'); 
var fs = require('fs');
var https = require('https');
var http = require('http');
var url = require("url");
var app = express();


var options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

app.get('/data1', function(req, res) {
    res.sendFile('data1', {root: __dirname });
});

app.get('/data2', function(req, res) {
    res.sendFile('data2', {root: __dirname });
});

app.get('/data3', function(req, res) {
    res.sendFile('data3', {root: __dirname });
});

app.get('/data4', function(req, res) {
    res.sendFile('data4', {root: __dirname });
});

app.get('/data5', function(req, res) {
    res.sendFile('data5', {root: __dirname });
});

app.get('/data6', function(req, res) {
    res.sendFile('data6', {root: __dirname });
});

app.get('/data7', function(req, res) {
    res.sendFile('data7', {root: __dirname });
});

app.get('/data8', function(req, res) {
    res.sendFile('data8', {root: __dirname });
});

app.get('/data9', function(req, res) {
    res.sendFile('data9', {root: __dirname });
});

http.createServer(app).listen(8579);
https.createServer(options, app).listen(8576);

