from socket import *
from time import sleep
from time import time
import sys
import numpy as np
import requests
import matplotlib.pyplot as plt

# ------------ HTTP ------------
print "------------ HTTP ------------"

http1 = np.zeros((100,4))
next=1.1
playback=0.052
for x in range(1,101):
    sleep(0.1)
    t1 = time()
    if next <=2. :
        print 'now request data1'
        r=requests.get('http://192.168.0.1:8579/data1')
    elif next <=3. :
        print 'now request data2'
        r=requests.get('http://192.168.0.1:8579/data2')
    elif next <=4. :
        print 'now request data3'
        r=requests.get('http://192.168.0.1:8579/data3')
    elif next <=5. :
        print 'now request data4'
        r=requests.get('http://192.168.0.1:8579/data4')
    elif next <=6. :
        print 'now request data5'
        r=requests.get('http://192.168.0.1:8579/data5')
    elif next <=7. :
        print 'now request data6'
        r=requests.get('http://192.168.0.1:8579/data6')
    elif next <=8. :
        print 'now request data7'
        r=requests.get('http://192.168.0.1:8579/data7')
    elif next <=9. :
        print 'now request data8'
        r=requests.get('http://192.168.0.1:8579/data8')
    elif next <=10. :
        print 'now request data9'
        r=requests.get('http://192.168.0.1:8579/data9')
    t4 = time()
    http1[x-1][0]=x
    http1[x-1][1]=next/playback/0.95
    http1[x-1][2]=int(r.headers['Content-Length'])/1024000./(t4-t1)
#    print 'x=', x, ' prediction=', http1[x-1][1], ' actual=',http1[x-1][2], 'next=', next
    if t4-t1>playback:
        http1[x-1][3]=0
    else:
        http1[x-1][3]=1
#    next = int(r.headers['Content-Length'])/1024000./(t4-t1)*0.95*0.055
    next=sum(http1[0:x,2])/x*0.95*playback
#print http1

if False:
    plt.plot(http1[:,0], http1[:,2], color="red", linewidth=2.5, linestyle="-", label="observation")
    plt.plot(http1[:,0], http1[:,1], color="blue", linewidth=2.5, linestyle="-", label="prediction")
    plt.legend(loc='center right')
    plt.xlabel("Packet Number")
    plt.ylabel("Transmission ratio")
    plt.savefig("cs925_hw5_ratio.pdf")

if True:
    plt.plot(http1[:,0], http1[:,3], color="red", linewidth=2.5, linestyle="-")
    plt.ylim([-0.5, 1.5])
    plt.xlabel("Packet Number")
    plt.ylabel("On time")
    plt.savefig("cs925_hw5_on_time.pdf")

plt.show()