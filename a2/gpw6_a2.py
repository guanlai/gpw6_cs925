import numpy as np
import random
import matplotlib.pyplot as plt

#   variable
#   t_que: the time when packet arrive queue
#   t_sys: the time when packet enter system
#   t_pro: the time of process
#   t_res: the residence time of a packet in the system
#   t_fin: the time when packet exit system
#   t_sim: the simulation time
mu = 1.
t_que = 0
t_sys = 0
t_pro = 0
t_res = 0
t_fin = 0
ans = np.zeros((100,3))

for lambd in range(1,101):
    count = 0
    result = 0.
    while count < 100000:
        count += 1
        t_que += random.expovariate(lambd/101.1)
        t_sys = max( t_que, t_fin )
        t_pro = random.expovariate(mu)
        t_fin = t_sys + t_pro
        t_res = t_fin - t_que
        result += t_res
    result /= count
    print lambd/101.1, result
    ans[lambd - 1][0] = lambd/101.1
    ans[lambd - 1][1] = result
    ans[lambd - 1][2] = 1/(1-lambd/101.1)

plt.plot(ans[:,0], ans[:,2], color="red", linewidth=2.5, linestyle="-", label="theory")
plt.plot(ans[:,0], ans[:,1], color="blue", linewidth=2.5, linestyle="-", label="simulation-100000 packets")
plt.legend(loc='upper left')
plt.xlabel("rho")
plt.ylabel("Residency time")
plt.savefig("residence_time_100000.pdf")
plt.show()