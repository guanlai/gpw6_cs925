import numpy as np
import random
import matplotlib.pyplot as plt

#   variable
#   t_que: the time when packet arrive queue
#   t_sys: the time when packet enter system
#   t_pro: the time of process
#   t_res: the residence time of a packet in the system
#   t_fin: the time when packet exit system
#   t_sim: the simulation time
mu = 1.
t_que = 0
t_sys = 0
t_pro = 0
t_res = 0
t_fin = 0
ans = np.zeros((200,3))

for lambd in range(1,201):
    q = []
    count = 0
    result = 0.
    while count < 10000:
        if t_que <= t_fin:
            count += 1
            result += len(q)
            t_que += random.expovariate(lambd/101.1)
            if len(q) < 3:
                q.append(t_que)
        else:
            t_fin += random.expovariate(mu)
            if len(q) > 0:
                q.pop(0)
    result /= count
    ans[lambd - 1][0] = lambd/101.1
    ans[lambd - 1][1] = result
    temp1 = lambd/101.1
    temp2 = temp1 * temp1
    temp3 = temp1 * temp1 * temp1
    ans[lambd - 1][2] = (3*temp3 + 2*temp2 + temp1)/(1+temp1+temp2+temp3)
#print ans[lambd - 1][0], ans[lambd - 1][1], ans[lambd - 1][2]
plt.plot(ans[:,0], ans[:,2], color="red", linewidth=2.5, linestyle="-", label="Theory")
plt.plot(ans[:,0], ans[:,1], color="blue", linewidth=2.5, linestyle="-", label="Simulation")
plt.legend(loc='upper left')
plt.xlabel("rho")
plt.ylabel("Average Number of Packets in System")
plt.savefig("cs925_hw3_10000.pdf")
plt.show()
