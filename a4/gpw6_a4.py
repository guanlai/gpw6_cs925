import numpy as np
import random
import matplotlib.pyplot as plt
import heapq

#define a class called 'Customer'
class Packet:
    def __init__(self, length, departure):
        self.size=length
        self.departure_time=departure

ans = np.zeros((30,3))

for num in range(1,31):
    WFQ_serv_rate = num * 1.
    RR_serv_rate = WFQ_serv_rate * 2

#prepare packets
#Initialise empty list to hold all data
#WFQ: the overall queue for WFQ, a heap by departure time
#RR: the overall round robin queue

#gererate queue, each type has 10000 packets
    a_arr = 0
    b_arr = 0
    a_depart = 0
    b_depart = 0
    a_len = 10.
    b_len = 20.
    
    WFQ=[]
    RR=[]
    for i in range(0,10000):
        a_depart = max( a_arr, a_depart ) + a_len / WFQ_serv_rate
        a_arr += random.expovariate(1)
        heapq.heappush(WFQ,(a_depart, Packet(a_len, a_depart)))

        a_depart = max( a_arr, a_depart ) + a_len / WFQ_serv_rate
        a_arr += random.expovariate(1)
        heapq.heappush(WFQ,(a_depart, Packet(a_len, a_depart)))

        b_depart = max( b_arr, b_depart ) + b_len / WFQ_serv_rate
        b_arr += random.expovariate(1)
        heapq.heappush(WFQ,(b_depart, Packet(b_len, b_depart)))

    a_arr = 0
    b_arr = 0
    a_depart = 0
    b_depart = 0

    for i in range(0,10000):
        a_depart = max(a_arr, b_depart) + a_len / RR_serv_rate
        a_arr += random.expovariate(1)
        RR.append(Packet(a_len, a_depart))

        b_depart = max(b_arr, a_depart) + b_len / RR_serv_rate
        b_arr += random.expovariate(1)
        RR.append(Packet(b_len, b_depart))

#process packets
    WFQ_A=0
    WFQ_B=0
    RR_A=0
    RR_B=0
    t_WFQ=0
    t_RR=0

    i = 0
    while t_WFQ < 10000 and i<29900:
        current = heapq.heappop(WFQ)
        t_WFQ = current[1].departure_time
        if current[1].size == a_len:
            WFQ_A += a_len
        else:
            WFQ_B += b_len
        i+=1

    i = 0
    while RR[i].departure_time < 10000 and i < 19900:
        if RR[i].size == a_len:
            RR_A += a_len
        else:
            RR_B += b_len
        i+=1

#    print "WFQ", WFQ_A * 1. / WFQ_B
#    print "RR", RR_A * 1./RR_B
    ans[num-1][0] = num
    ans[num-1][1] = WFQ_A * 1. / WFQ_B
    ans[num-1][2] = RR_A * 1./RR_B

plt.plot(ans[:,0], ans[:,2], color="red", linewidth=2, linestyle="-", label="Round Robin")
plt.plot(ans[:,0], ans[:,1], color="blue", linewidth=2, linestyle="-", label="WFQ")
plt.legend(loc='upper right')
plt.xlabel("Service rate")
plt.ylabel("Ratio of Bandwidth used by Two Types of Packets")
plt.savefig("cs925_hw4.pdf")
plt.show()


